##### Worldline Assessment 4 - The objective of this assignment is to automate the configuration management of infrastructure resources provisioned using Terraform using Puppet.


1. Create ec2 instance from AWS console : puppet-master


2. Install puppet-server in it and configure site.pp

- sudo nano /etc/hosts
- <Puppet_Master_Private_IP> puppet
- curl -O https://apt.puppetlabs.com/puppet6-release-bionic.deb
- sudo dpkg -i puppet6-release-bionic.deb
- sudo apt-get update
- sudo apt-get install puppetserver -y
- sudo nano /etc/default/puppetserver
    JAVA_ARGS = "-Xms512m -Xms512m"
- sudo ufw allow 8140
- sudo systemctl enable puppetserver.service
- sudo systemctl start puppetserver.service
- sudo systemctl status puppetserver.service
- sudo nano /etc/puppetlabs/code/environments/production/manifests/site.pp
**THIS IS DONE TO CONFIGURE PROVISIONED INFRASTRUCTURE COMPONENTS i.e, to install various packages and dependencies **
    ```
    node all{
        package { 'nginx': ensure => installed, }
        package { 'apache2': ensure => installed, }
    }
    ```


3. Install Terraform

- sudo apt-get update && sudo apt-get install -y gnupg software-properties-common
- wget -O- https://apt.releases.hashicorp.com/gpg | \
gpg --dearmor | \
sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg > /dev/null
- gpg --no-default-keyring \
--keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg \
--fingerprint
- echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
sudo tee /etc/apt/sources.list.d/hashicorp.list
- sudo apt update
- sudo apt-get install terraform


- mkdir terraform
- nano ec2.tf

Write a terraform file to create instance and embed ansible provisioner inside it.
Terraform Code: ec2.tf

```
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

provider "aws" {
  region     = "us-west-1"
  access_key = "AKIAXEW5EO3VWIKTUCUM"
  secret_key = "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"
}


resource "aws_security_group" "my_security_group" {
  ingress {
    protocol="tcp"
    from_port = "80"
    to_port="80"
    cidr_blocks=["0.0.0.0/0", "::/0"]
  }
  ingress {
    protocol = "tcp"
    from_port = "22"
    to_port = "22"
    cidr_blocks=["0.0.0.0/0", "::/0"]
  }
  ingress {
    protocol = "tcp"
    from_port = "443"
    to_port = "443"
    cidr_blocks=["0.0.0.0/0", "::/0"]
  }
  egress {
    from_port=0
    to_port=0
    protocol="-1"
    cidr_blocks=["0.0.0.0/0"]
  }
  tags={
    Name="my_security_group"
  }
}

resource "aws_instance" "AWSinstance" {
  ami="ami-07619059e86eaaaa2"
  instance_type = "t2.medium"
  availability_zone = "us-west-1c"
  key_name = "puppet8"
  tags = {
    Name = "Terraform-ubuntu"
  }
}
resource "aws_instance" "AWSinstance" {
  provisioner "remote-exec" {
    inline = ["sudo hostnamectl set-hostname cloudEc2.technix.com"]
    connection {
      host = aws_instance.AWSinstance.public_dns
      type = "ssh"
      user = "ec2-user"
      private_key = file("./puppet8.pem")
    }
  }
  provisioner "local-exec" {
    command = "echo ${aws_instance.AWSinstance.public_dns} > inventory"
  }
}
  

output "ip" {
  value = aws_instance.AWSinstance.public_ip
}
output "publicName" {
  value = aws_instance.AWSinstance.public_dns
}
```

- terraform init
- terraform validate
- terraform plan
- terraform apply



4. Install Ansible

- sudo apt install software-properties-common
- sudo apt-add-repository ppa:ansible/ansible
- sudo apt update
- sudo apt install ansible
- ssh-keygen

**USING ANSIBLE WE CAN CONNECT NEW TERRAFORM RESOURCES TO THE PUPPET MASTER**

- sudo nano puppet_launch.yaml
---
```
- hosts: all
  name: execute puppet installation script
  tasks:
     - name: puppet agent installation
       script: /home/ubuntu/install.sh
```


5. Write a shell script to install puppet agent on new machines
- sudo nano install.sh

```
!#/bin/bash

echo <private_master_ipv4> puppet >> etc/hosts.txt
curl -O https://apt.puppetlabs.com/puppet6-release-bionic.deb
sudo dpkg -i puppet6-release-bionic.deb
sudo apt-get update
sudo apt-get install puppet-agent -y

sudo systemctl enable puppet
sudo systemctl restart puppet
sudo systemctl status puppet
```



6. Following files should be there in terraform directory

- ec2.tf
- puppet_launch.yaml
- aws_instance_key.pem
- inventory

- ansible.cfg
```
[defaults]
inventory = ./inventory
deprecation_warnings = False
remote_user = ec2-user
host_key_checking = False
private_key_file = ./puppet8.pem

[privilege_escalation]
become = true
become_method = sudo
become_user = root
become_ask_pass = False
```

**Hence, we have automated the configuration management of
infrastructure resources provisioned using Terraform using Puppet**

**
WE CREATED THE CLOUD RESOURCES THROUGH TERRAFORM
WE INSTALLED PUPPET AGENT ON NEW RESOURCES WITH ANSIBLE AND SHELL SCRIPTING
WE INSTALLED VARIOUS PACKAGES USING PUPPET MANIFESTS
**
